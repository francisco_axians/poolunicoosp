from openpyxl import load_workbook 
import ipaddress 
import os
import paramiko
import time
import tkinter as tk
from tkinter import filedialog

root = tk.Tk()
root.withdraw()

file_path = filedialog.askopenfilename()

def connectSSH():
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect('62.36.193.80', username='hua-sop', password='hua2014sop', timeout=100)
    console = ssh.invoke_shell()
    console.keep_this = ssh
    return ssh

try:
    ssh = connectSSH()
except:
    print("Error al realizar la conexión SSH contra m2gred")

wb = load_workbook(file_path,data_only=True)
sheet_ranges = wb['Plan Despliegue']
ws1 = wb.active
max_row = ws1.max_row
datos = []
TPAux = ""
aggAUX = ""
for row in range(2,max_row+1):
    TP = ws1.cell(row = row,column = 1).value
    if not TP is None:
        agg = ws1.cell(row = row,column = 3).value.strip().upper()
        if not (aggAUX == agg) or not (TP == TPAux):
            del datos
            datos = []
            if os.path.exists(TP+" - " + agg +".txt"):
                os.remove(TP+" - " + agg +".txt")
            files = open(TP+" - " + agg +".txt","a+")
            giaddr = ws1.cell(row = row,column = 11).value
            poolDyn = ws1.cell(row = row,column = 12).value
            poolFix = ws1.cell(row = row,column = 13).value
            poolSus = ws1.cell(row = row,column = 14).value
        date = ws1.cell(row = row,column = 2).value
        OLT = ws1.cell(row = row,column = 4).value
        lag = ws1.cell(row = row,column = 5).value.replace("Eth-Trunk","").replace("lag-","").strip()
        tieneIPV6 = ws1.cell(row = row,column = 6).value
        oldGiadd = ws1.cell(row = row,column = 7).value
        oldDyn = ws1.cell(row = row,column = 8).value
        oldFix = ws1.cell(row = row,column = 9).value
        oldSus = ws1.cell(row = row,column = 10).value
        datos.append({
            'TP' : TP,
            'date' : date,
            'agg' : agg,
            'OLT' : OLT,
            'lag' : lag,
            'tieneIPV6' : tieneIPV6,
            'oldGiadd' : oldGiadd,
            'oldDyn' : oldDyn,
            'oldFix' : oldFix,
            'oldSus' : oldSus,
            'giaddr' : giaddr,
            'poolDyn' : poolDyn,
            'poolFix' : poolFix,
            'poolSus' : poolSus
        })
        aggAUX = str(ws1.cell(row = row+1,column = 3).value).strip()
        TPAux = ws1.cell(row = row+1,column = 1).value
        leaseTime = "##########################\n"
        leaseTime = leaseTime + "###Solicitud Lease Time###\n"
        leaseTime = leaseTime + "##########################\n"
        leaseTime = leaseTime + "Giaddress          -   Pool        -   Time\n"
        for y in datos:
            arrayoldpoolDyn = str(y["oldDyn"]).strip().splitlines()
            arrayoldpoolFix = str(y["oldFix"]).strip().splitlines()
            arrayoldpoolSus = str(y["oldSus"]).strip().splitlines()
            for pool in arrayoldpoolDyn:
                leaseTime = leaseTime + str(y["oldGiadd"]) + "      -   " + pool + "        -   60\n"
            for pool in arrayoldpoolFix:
                leaseTime = leaseTime + str(y["oldGiadd"]) + "      -   " + pool + "        -   60\n"
            for pool in arrayoldpoolSus:
                leaseTime = leaseTime + str(y["oldGiadd"]) + "      -   " + pool + "        -   60\n"
        if not (aggAUX == agg) or not (TPAux == TP):
            if agg.find("LS") >= 0:
                stdin, stdout, stderr = ssh.exec_command('telnet ' + agg + '\n')
                print("lanzado telnet")
                stdin.write('RZCD9491\n')
                stdin.write('Orange01\n \n')
                print ("conectado a "+agg)
                print ("Lanzando comandos")
                stdin.write('environment no more\n')
                stdin.write('admin display-config\n')
                stdin.write('show service sap-using sap lag-' + lag + ' | match 550 \n')
                stdin.write('show filter ip 150\n')
                stdin.write('show router interface\n')
                if not aggAUX == agg:
                    stdin.write('logout\n')
                print ("comandos ejecutados. Pasando a procesar")
                data = ""
                borradoIES = ""
                data = stdout.read()
                lineas = data.splitlines()
                vuelta = 0
                ipv6 = ""
                borradoFiltros = ""
                prefix = ""
                configuration = ""
                for linea in lineas:
                    linea = linea.decode('iso-8859-1')
                    for y in datos:
                        arrayoldSus = y["oldSus"].splitlines()
                        for x in arrayoldSus:
                            if linea.find("Up   Up") >= 0:
                                ies = linea.split()[1]
                            elif linea.find("Src. IP             : "+x) >= 0:
                                entry = lineas[vuelta-3].split()[2]
                                borradoFiltros = borradoFiltros + 'configure filter ip-filter 150 no entry ' + str(entry).replace("b'","").replace("'","") + '\n'
                            elif linea.find("/128") >=0:
                                ipv6 = linea.split()[0].replace("/128","")
                    vuelta = vuelta + 1
                checks = "----------------------------------------------------------------------------------------\n"
                checks = checks + " COMPROBACIONES GENERALES " + agg + "\n"
                checks = checks + "----------------------------------------------------------------------------------------\n"
                checks = checks + "environment no more\n"
                checks = checks + "admin display-config\n"
                checks = checks + "\n"
                checks = checks + "show log log-id 99 severity critical\n"
                checks = checks + "show log log-id 99 severity major\n"
                checks = checks + "\n"
                checks = checks + "show router interface\n"
                checks = checks + "show router bgp summary\n"
                checks = checks + "show router bgp neighbor <DN1_LOOPBACK> advertised-routes\n"
                checks = checks + "show router bgp neighbor <DN2_LOOPBACK> advertised-routes\n"
                checks = checks + "\n"
                checks = checks + "show router isis interface\n"
                checks = checks + "show router isis adjacency\n"
                checks = checks + "show router ospf interface\n"
                checks = checks + "show router ospf neighbor\n"
                checks = checks + "\n"
                checks = checks + "show service service-using vprn\n"
                checks = checks + "show router route-table summary\n"
                checks = checks + "show router service-name " + ies + "  route-table summary\n"
                checks = checks + "show router service-name serverz route-table summary\n"
                checks = checks + "show service service-using | match \"Matching Services :\"\n"
                checks = checks + "show service service-using | match \"Up   Up\" | count \n"
                checks = checks + "show service service-using | match Down | count \n"
                checks = checks + "show service sap-using | match \"Number of SAPs :\"\n"
                checks = checks + "show service sap-using | match \"Up   Up\" | count \n"
                checks = checks + "show service sap-using | match Down | count\n"
                checks = checks + "show service active-subscribers summary\n"
                checks = checks + "\n"
                checks = checks + "show lag description\n"
                userCheck = ""
                for y in datos:
                    userCheck = userCheck + 'show service id ' + ies + ' dhcp lease-state sap lag-' + str(y["lag"]) + ':550\n'
                    checks = checks + "monitor lag " + str(y["lag"]) + " rate repeat 5\n"
                checks = checks + "show service id " + ies + "  interface  \n"
                checks = checks + "show service id " + ies + "  sap  \n"
                checks = checks + "----------------------------------------------------------------------------------------\n"
                checks = checks + " Usuarios actuales \n"
                checks = checks + "----------------------------------------------------------------------------------------\n"
                checks = checks + "show service id " + ies + "  dhcp summary \n"
                checks = checks + "show service id " + ies + "  dhcp6 summary\n"
                checks = checks + userCheck
                checks = checks + "----------------------------------------------------------------------------------------\n"
                for y in datos:
                    borradoIES = borradoIES + '\n########################\n####Desconfiguración del servicio de la OLT ' + str(y["OLT"]) + ' ######\n########################\n'
                    borradoIES = borradoIES + 'configure service ies '+ ies +' subscriber-interface "FTTH-' + str(y["OLT"]) + '" group-interface "Lag-'+ str(y["lag"]) +'-' + str(y["OLT"]) + '" sap lag-'+ str(y["lag"]) +':550 shutdown\n'
                    borradoIES = borradoIES + 'configure service ies '+ ies +' subscriber-interface "FTTH-' + str(y["OLT"]) + '" group-interface "Lag-'+ str(y["lag"]) +'-' + str(y["OLT"]) + '" no sap lag-'+ str(y["lag"]) +':550\n'
                    borradoIES = borradoIES + 'configure service ies '+ ies +' subscriber-interface "FTTH-' + str(y["OLT"]) + '" group-interface "Lag-'+ str(y["lag"]) +'-' + str(y["OLT"]) + '" shutdown\n'
                    borradoIES = borradoIES + 'configure service ies '+ ies +' subscriber-interface "FTTH-' + str(y["OLT"]) + '" group-interface no "Lag-'+ str(y["lag"]) +'-' + str(y["OLT"]) + '"\n'
                    borradoIES = borradoIES + 'configure service ies '+ ies +' subscriber-interface "FTTH-' + str(y["OLT"]) + '" shutdown\n'
                    borradoIES = borradoIES + 'configure service ies '+ ies +' no subscriber-interface "FTTH-' + str(y["OLT"]) + '"\n\n\n'
                configuration = configuration + checks
                configuration = configuration + borradoIES
                configuration = configuration + borradoFiltros
                configuration = configuration + '\n\nconfigure filter ip-filter 150 \n'
                entry = 300
                poolSusArray = poolSus.splitlines()
                for x in poolSusArray:
                    configuration = configuration + '   entry ' + str(entry) + ' create\n'
                    configuration = configuration + '     description "Blacklist ' + x + '"\n'
                    configuration = configuration + '     match \n'
                    configuration = configuration + '       src-ip ' + x + '\n'
                    configuration = configuration + '     exit \n'
                    configuration = configuration + '     action\n'
                    configuration = configuration + '       drop\n'
                    configuration = configuration + '     exit\n'
                    configuration = configuration + '   exit \n'
                    entry = entry + 1
                configuration = configuration + 'exit all \n'
                configuration = configuration + '\n'
                configuration = configuration + '\n'
                configuration = configuration + 'configure service ies '+ ies +'\n'
                configuration = configuration + '    description "IES-FTTH"\n'
                configuration = configuration + '####Revisar si la interfaz está creada####\n'
                configuration = configuration + '    #interface "Accept-DHCP-Frontal" create\n'
                configuration = configuration + '    #    address 10.34.2.238/32\n'
                configuration = configuration + '    #    dhcp\n'
                configuration = configuration + '    #        server 81.52.127.249 81.52.127.250 81.52.127.253 81.52.127.254\n'
                configuration = configuration + '    #    no shutdown\n'
                configuration = configuration + '    #    exit\n'
                configuration = configuration + '    #    ipv6\n'
                configuration = configuration + '    #        dhcp6-relay\n'
                configuration = configuration + '    #            server 2a01:c000:14::1\n'
                configuration = configuration + '    #            server 2a01:c000:14:1::1\n'
                configuration = configuration + '    #        exit\n'
                configuration = configuration + '    #    exit\n'
                configuration = configuration + '    #    loopback\n'
                configuration = configuration + '    #exit\n'
                configuration = configuration + '    interface "LB-FTTH-DHCPv4" create\n'
                configuration = configuration + '        description "Loopback FTTH relay-proxy DHCPv4 for the LS"\n'
                configuration = configuration + '        address ' + giaddr + '/32\n'
                configuration = configuration + '        loopback\n'
                configuration = configuration + '    exit\n'
                configuration = configuration + '    subscriber-interface "FTTH-DIRECT" create\n'
                configuration = configuration + '        description "FTTH-DIRECT"\n'
                arraypoolDyn = str(poolDyn).strip().splitlines()
                arraypoolFix = str(poolFix).strip().splitlines()
                arraypoolSus = str(poolSus).strip().splitlines()
                for pool in arraypoolDyn:
                    net4 = ipaddress.IPv4Network(pool.strip())
                    configuration = configuration + '        address '+str(net4[-2])+'/'+str(net4.prefixlen)+'\n'
                    prefix = prefix + "#    	prefix " + pool + ' longer \n'
                for pool in arraypoolFix:
                    net4 = ipaddress.IPv4Network(pool.strip())
                    configuration = configuration + '        address '+str(net4[-2])+'/'+str(net4.prefixlen)+'\n'
                    prefix =  prefix + "#    	prefix " + pool + ' longer \n'
                for pool in arraypoolSus:
                    net4 = ipaddress.IPv4Network(pool.strip())
                    configuration = configuration + '        address '+str(net4[-2])+'/'+str(net4.prefixlen)+'\n'
                    prefix =  prefix + "#    	prefix " + pool + ' longer \n'
                configuration = configuration + '        ipv6\n'
                configuration = configuration + '            delegated-prefix-len 56\n'
                configuration = configuration + '            allow-unmatching-prefixes\n'
                configuration = configuration + '        exit\n'
                configuration = configuration + '    exit\n'
                configuration = configuration + '    service-name "IES-FTTH"\n'
                configuration = configuration + '    no shutdown\n'
                configuration = configuration + 'exit all\n'
                configuration = configuration + '\n'
                configuration = configuration + '\n'
                rollback = "#----------------------------------------------------------------------------------------\n"
                rollback = rollback + "# Rollback\n"
                rollback = rollback + "#----------------------------------------------------------------------------------------\n"
                for y in datos:
                    rollback = rollback + 'configure service ies "IES-FTTH" subscriber-interface "FTTH-DIRECT" group-interface "Lag-'+ str(y["lag"]) +'-'+ str(y["OLT"]) +'" sap '+ str(y["lag"]) +':550 shutdown\n'
                    rollback = rollback + 'configure service ies "IES-FTTH" subscriber-interface "FTTH-DIRECT" group-interface "Lag-'+ str(y["lag"]) +'-'+ str(y["OLT"]) +'" no sap '+ str(y["lag"]) +':550\n'
                    rollback = rollback + 'configure service ies "IES-FTTH" subscriber-interface "FTTH-DIRECT" group-interface "Lag-'+ str(y["lag"]) +'-'+ str(y["OLT"]) +'" shutdown\n'
                    rollback = rollback + 'configure service ies "IES-FTTH" subscriber-interface "FTTH-DIRECT" no group-interface "Lag-'+ str(y["lag"]) +'-'+ str(y["OLT"]) +'"\n'
                    rollback = rollback + 'configure service ies "IES-FTTH" subscriber-interface "FTTH-DIRECT" shutdown\n'
                    rollback = rollback + 'configure service ies "IES-FTTH" no subscriber-interface "FTTH-DIRECT"\n'
                    configuration = configuration + 'configure service ies "IES-FTTH" subscriber-interface "FTTH-DIRECT"\n'
                    configuration = configuration + '    group-interface "Lag-'+ str(y["lag"]) +'-'+ str(y["OLT"]) +'" create\n'
                    configuration = configuration + '        description "Lag-'+ str(y["lag"]) +'-'+ str(y["OLT"]) +'"\n'
                    configuration = configuration + '        enable-ingress-stats\n'
                    configuration = configuration + '        ipv6\n'
                    configuration = configuration + '            router-advertisements\n'
                    configuration = configuration + '                managed-configuration\n'
                    configuration = configuration + '                other-stateful-configuration\n'
                    configuration = configuration + '                prefix-options\n'
                    configuration = configuration + '                    autonomous\n'
                    configuration = configuration + '                exit\n'
                    configuration = configuration + '                no shutdown\n'
                    configuration = configuration + '            exit\n'
                    configuration = configuration + '            dhcp6\n'
                    configuration = configuration + '                option\n'
                    configuration = configuration + '                    interface-id string "'+agg+'#'+ies+'#FTTH-DIRECT#Lag-'+ str(y["lag"]) +'"\n'
                    configuration = configuration + '                exit\n'
                    configuration = configuration + '                relay\n'
                    configuration = configuration + '                    source-address ' + ipv6 + '\n'
                    configuration = configuration + '                    link-address ' + ipv6 + '\n'
                    configuration = configuration + '                    server 2a01:c000:14::1\n'
                    configuration = configuration + '                    server 2a01:c000:14:1::1\n'
                    configuration = configuration + '                    shutdown\n'
                    configuration = configuration + '                exit\n'
                    configuration = configuration + '            exit\n'
                    configuration = configuration + '        exit\n'
                    configuration = configuration + '        arp-populate\n'
                    configuration = configuration + '        dhcp\n'
                    configuration = configuration + '            proxy-server\n'
                    configuration = configuration + '                lease-time min 10 override\n'
                    configuration = configuration + '                no shutdown\n'
                    configuration = configuration + '            exit\n'
                    configuration = configuration + '            server 81.52.127.248 81.52.127.252\n'
                    configuration = configuration + '            trusted\n'
                    configuration = configuration + '            lease-populate 16383\n'
                    configuration = configuration + '#           lease-populate 131071 - Para SR-7\n'
                    configuration = configuration + '            relay-proxy release-update-src-ip\n'
                    configuration = configuration + '            gi-address ' + giaddr + ' src-ip-addr\n'
                    configuration = configuration + '            no shutdown\n'
                    configuration = configuration + '        exit\n'
                    configuration = configuration + '        sap '+ str(y["lag"]) +':550 create\n'
                    configuration = configuration + '            description "OLT '+ str(y["OLT"]) +'"\n'
                    configuration = configuration + '            sub-sla-mgmt\n'
                    configuration = configuration + '            def-inter-dest-id string "FTTH"\n'
                    configuration = configuration + '            def-sub-id use-auto-id\n'
                    configuration = configuration + '            def-sub-profile "FTTH-SUB"\n'
                    configuration = configuration + '            def-sla-profile "FTTH-SLA"\n'
                    configuration = configuration + '            multi-sub-sap 16383\n'
                    configuration = configuration + '#           multi-sub-sap 131071 - Para SR-7\n'
                    configuration = configuration + '            no shutdown\n'
                    configuration = configuration + '        exit\n'
                    configuration = configuration + '    exit\n'
                    configuration = configuration + 'exit all\n'
                configuration = configuration + '\n'
                configuration = configuration + '\n'
                configuration = configuration + '###Revisar la configuración por si no estuvieran ya añadidos los pooles\n'
                configuration = configuration + '#configure router policy\n'
                configuration = configuration + '#    abort\n'
                configuration = configuration + '#    begin\n'
                configuration = configuration + '#    prefix-list "Pooles-VoIP-HSI"\n'
                configuration = configuration + prefix
                configuration = configuration + '#        prefix ' + giaddr + '/32 exact\n'
                configuration = configuration + '#    exit\n'
                configuration = configuration + '#    prefix-list "IPV6-System-addr"\n'
                configuration = configuration + '#        prefix ' + ipv6 + '/128 exact\n'
                configuration = configuration + '#    exit\n'
                configuration = configuration + '#    commit\n'
                configuration = configuration + '#exit all\n'
                configuration = leaseTime + configuration
                configuration = configuration + "\n\n\n" + checks
                configuration = configuration + userCheck
                configuration = configuration + "\n\n\n" + rollback 
                files.write(configuration)
                aggAUX = str(ws1.cell(row = row,column = 3).value).strip().upper()
                TPAux = ws1.cell(row = row,column = 1).value
            elif agg.find("ROU") >= 0 or agg.find("RA") >= 0:
                if agg.find("ROU") >= 0:
                    agg = agg.upper()
                    stdin, stdout, stderr = ssh.exec_command('telnet ' + agg + '\n')
                    print("lanzado telnet")
                    stdin.write('LTHF1575\n')
                    stdin.flush()
                    time.sleep(2)
                    stdin.write('Orange123\n \n')
                    stdin.flush()
                    print ("conectado a "+agg)
                    print ("Lanzando comandos")
                else:
                    agg = agg.upper()
                    stdin, stdout, stderr = ssh.exec_command('ls -la\n')
                    data = stdout.read()
                    lineas = data.splitlines()
                    for linea in lineas:
                        print(linea)
                    print("lanzado ssh")
                    time.sleep(2)
                    stdin.write('ssh -l fgarcia ' + agg + '\nOrange02\n')
                    print ("conectadooo a "+agg)
                    print ("Lanzando comandos")
                stdin.write('s 0 t \n')
                stdin.flush()
                stdin.write('dis cur \n')
                stdin.flush()
                stdin.write('dis int des | i 550\n')
                stdin.flush()
                stdin.write('dis cur | i apply community\n')
                stdin.flush()              
                if not aggAUX == agg:
                    time.sleep(1)
                    stdin.write('q\n')
                    stdin.flush()
                print ("comandos ejecutados. Pasando a procesar")
                data = ""
                configuration = ""
                borradoPooles = "###############\nBorrado de los pooles\n################\n\n"
                borradoDSG = "###############\nBorrado de los dhcp-server-group\n################\n\n"
                borradoInt = "###############\nBorrado de las interfaces\n################\n\n"
                borradoCli = "###############\nBorrado de los clientes\n################\naaa\n"
                borradoDom = "###############\nBorrado del dominio\n################\n\naaa\n"
                data = stdout.read()
                lineas = data.splitlines()
                comm = "<COMMUNITY>"
                idRoute = "<ID>"
                idRouteInt = 10
                arrayPoolFinal = []
                for y in datos:
                    borradoPrefix = "###############\nBorrado de las prefix\n################\n\n\n"
                    borradoACL = "#####\nBorrado de las entradas en la ACL (suspendidos)\n########\n\n\nacl number 3151\n"
                    arrayOldDyn = y["oldDyn"].splitlines()
                    for pool in arrayOldDyn:
                        arrayPoolFinal.append(pool.split("/")[0])
                    arrayOldFix = y["oldFix"].splitlines()   
                    for pool in arrayOldFix:
                        arrayPoolFinal.append(pool.split("/")[0]) 
                    arrayOldSus = y["oldSus"].splitlines()
                    for pool in arrayOldSus:
                        arrayPoolFinal.append(pool.split("/")[0]) 
                    borradoDominio = ""
                    borradoDomFinal = ""
                    rollbackPrefix = "###Creación de las prefix antiguas###\n"
                    for linea in lineas:
                        linea = linea.decode('iso-8859-1')
                        if linea.find(" domain "+ str(y["OLT"]).lower() +"_o") >= 0:
                            borradoDom = borradoDom + linea + "\n"
                            borradoDomFinal = borradoDomFinal + " undo " + linea + "\nq\n"
                        elif linea.find("ip pool "+ str(y["OLT"]).lower() +"_o") >= 0:
                            borradoPooles = borradoPooles + "undo " + linea.replace("bas remote","") + "\n"
                            borradoDominio = borradoDominio + "    undo " + linea.replace("ip pool", "ip-pool").replace("bas remote","") + "\n"
                        elif linea.find("dhcp-server group "+ str(y["OLT"]).lower()) == 0:
                            borradoDSG = borradoDSG + "undo " + linea + "\n"
                        elif linea.find("| OSP |") >= 0:
                            linea = linea.replace("                                               ","")
                            if linea.find(str(y["OLT"]).lower().upper()) >= 0:
                                if linea.find("description") == -1:
                                    if linea.find("Eth-Trunk") >= 0:
                                        borradoInt = borradoInt + linea[0:15] + "\n undo bas \nq \n\n"
                                        borradoCli = borradoCli + "  cut access-user "  + linea[0:15] + "\n"
                        elif linea.find("apply community 12479") >= 0:
                            comm = linea.split()[2]
                        elif linea.find("route-policy DIRECT_V4_TO_BGP permit") >= 0:
                            idRoute = linea.split()[4]
                            idRouteInt = int(idRoute) + 10
                    for pool in arrayPoolFinal:
                        for linea in lineas:
                            linea = linea.decode('iso-8859-1')
                            if linea.find(pool) >= 0:
                                if linea.find("rule") >= 0:
                                    borradoACL = borradoACL + "  undo " + linea + "\n"
                                    idLinea = linea.split()[1]
                                else:
                                    rollbackPrefix = rollbackPrefix + linea + "\n"    
                                    borradoPrefix = borradoPrefix + "undo " + linea + "\n"
                    borradoDom = borradoDom + borradoDominio + "  q\n"
                    borradoDom = borradoDom + borradoDomFinal
                vuelta = 1
                ipprefix = ""
                arraypoolDyn = str(poolDyn).strip().splitlines()
                arraypoolFix = str(poolFix).strip().splitlines()
                arraypoolSus = str(poolSus).strip().splitlines()
                resultPoolFix = ""
                resultPoolDyn = ""
                resultPoolSus = ""

                checks = '#---------------------------------- \n'
                checks = checks + '#Previous checks in ' + agg + '\n'
                checks = checks +'#---------------------------------- \n'
                checks = checks +'s 0 t \n'
                checks = checks + 'display clock\n'
                checks = checks + 'display current-configuration\n'
                checks = checks + 'display device pic-status\n'
                checks = checks + 'display device\n'
                checks = checks + 'display alarm all\n'
                checks = checks + 'display interface brief\n'
                checks = checks + 'display ip interface brief\n'
                checks = checks + 'display arp all\n'
                checks = checks + 'display bgp peer\n'
                checks = checks + 'display ip routing-table statistics\n'
                checks = checks + 'display ipv6 routing-table statistics\n'
                checks = checks + 'display isis peer\n'
                checks = checks + 'display ospf peer\n'
                checks = checks + 'display mpls ldp peer\n'
                checks = checks + 'display mpls ldp session\n'
                checks = checks + 'display bfd session all\n'
                checks = checks + 'dis ip-pool pool-usage all\n'
                checks = checks + 'dis bas\n'
                checks = checks + 'dis domain\n'
                checks = checks + 'display ds-LIte tunnel table cpe 2A01:C500:: prefix-length 28\n'
                checks = checks + '\n'
                userCheck = ""
                userCheck = userCheck + "\n##----Comprobacion de los usuarios actuales----###\n"
                for y in datos:
                    checks = checks + 'disp interface eth-trunk' + str(y["lag"]) + ' | i utilization\n'
                    userCheck = userCheck + 'display access-user interface eth-trunk' + str(y["lag"]) + '\n'
                checks = checks + userCheck + "\n##############################\n"
                checks = checks + '\n'
                checks = checks + 'disp bgp peer\n'
                checks = checks + 'dis bgp routing-table peer <DN1_LOOPBACK> advertised-routes \n'
                checks = checks + 'dis bgp routing-table peer <DN2_LOOPBACK> advertised-routes \n'
                checks = checks + 'dis bgp routing-table peer <DN1_LOOPBACK> received-routes stat\n'
                checks = checks + 'dis bgp routing-table peer <DN2_LOOPBACK> received-routes stat\n'
                checks = checks + '###########\n'
                configuration = configuration + '\n#-------------------------------------- \n'
                configuration = configuration + '#IMPLEMENTACIÓN \n'
                configuration = configuration + '#--------------------------------------  \n'
                configuration = configuration + '\n'
                configuration = configuration + '##\n'
                configuration = configuration + 'interface LoopBack2100\n'
                configuration = configuration + ' description OSP Pool Loopback\n'
                configuration = configuration + ' ip address ' + str(giaddr) + ' 255.255.255.255\n'
                configuration = configuration + ' \n'
                configuration = configuration + 'dhcp-server group '+ agg.lower() + '_os4\n'
                configuration = configuration + ' dhcp-server 81.52.127.248  \n'
                configuration = configuration + ' dhcp-server 81.52.127.252  \n'
                configuration = configuration + ' dhcp-server algorithm polling check-loose\n'
                configuration = configuration + ' dhcp-server source interface LoopBack2100\n'
                configuration = configuration + ' dhcp-server giaddr interface LoopBack2100\n'
                configuration = configuration + ' \n'
                configuration = configuration + '#\n'
                configuration = configuration + '\n'
                rollbackPool = ""
                rollback = "#----------------------------------------------------------------------------------------\n"
                rollback = rollback + "# Rollback\n"
                rollback = rollback + "#----------------------------------------------------------------------------------------\n"
                postChecks = "#----------------------------------------------------------------------------------------\n"
                postChecks = postChecks + "# PostChecks\n"
                postChecks = postChecks + "#----------------------------------------------------------------------------------------\n"
                postChecks = postChecks + checks
                for pool in arraypoolFix:
                    postChecks = postChecks + "display bgp routing-table peer <DN1_LOOPBACK> advertised-routes " + pool.split("/")[0] + " " + pool.split("/")[1] + " \n"
                    net4 = ipaddress.IPv4Network(pool.strip())
                    if vuelta >= 10:
                        configuration = configuration + 'ip pool ftth_'+ agg.lower() + '_fix_o4' + str(vuelta) + ' bas remote\n'
                        resultPoolFix = resultPoolFix + ' ip-pool ftth_'+ agg.lower() + '_fix_o4' + str(vuelta) + '\n'
                        rollbackPool = rollbackPool + 'undo ip-pool ftth_'+ agg.lower() + '_fix_o4' + str(vuelta) + '\n'
                    else:
                        configuration = configuration + 'ip pool ftth_'+ agg.lower() + '_fix_o40' + str(vuelta) + ' bas remote\n'
                        resultPoolFix = resultPoolFix + ' ip-pool ftth_'+ agg.lower() + '_fix_o40' + str(vuelta) + '\n'
                        rollbackPool = rollbackPool + ' undo ip pool ftth_'+ agg.lower() + '_fix_o40' + str(vuelta) + '\n'
                    configuration = configuration + ' gateway '+str(net4[-2])+' '+str(net4.prefixlen)+'\n'
                    configuration = configuration + ' dhcp-server group '+ agg.lower() + '_os4\n'
                    configuration = configuration + ' remote-ip lease manage\n'
                    configuration = configuration + '\n'
                    ipprefix = ipprefix + 'ip ip-prefix FTTH-V4-POOL permit '+str(net4[0])+' '+str(net4.prefixlen)+'\n'
                    rollback = rollback + 'undo ip ip-prefix FTTH-V4-POOL permit '+str(net4[0])+' '+str(net4.prefixlen)+'\n'
                    vuelta += 1
                vuelta = 1
                for pool in arraypoolDyn:
                    postChecks = postChecks + "display bgp routing-table peer <DN1_LOOPBACK> advertised-routes " + pool.split("/")[0] + " " + pool.split("/")[1] + " \n"
                    net4 = ipaddress.IPv4Network(pool.strip())
                    if vuelta >= 10:
                        configuration = configuration + 'ip pool ftth_'+ agg.lower() + '_dyn_o4' + str(vuelta) + ' bas remote\n'
                        resultPoolDyn = resultPoolDyn + 'ip-pool ftth_'+ agg.lower() + '_dyn_o4' + str(vuelta) + '\n'
                        rollbackPool = rollbackPool + ' undo ip-pool ftth_'+ agg.lower() + '_dyn_o4' + str(vuelta) + '\n'
                    else:
                        configuration = configuration + 'ip pool ftth_'+ agg.lower() + '_dyn_o40' + str(vuelta) + ' bas remote\n'
                        resultPoolDyn = resultPoolDyn + 'ip-pool ftth_'+ agg.lower() + '_dyn_o40' + str(vuelta) + '\n'
                        rollbackPool = rollbackPool + 'undo ip-pool ftth_'+ agg.lower() + '_dyn_o40' + str(vuelta) + '\n'
                    configuration = configuration + ' gateway '+str(net4[-2])+' '+str(net4.prefixlen)+'\n'
                    configuration = configuration + ' dhcp-server group '+ agg.lower() + '_os4\n'
                    configuration = configuration + ' remote-ip lease manage\n'
                    configuration = configuration + '\n'
                    ipprefix = ipprefix + 'ip ip-prefix FTTH-V4-POOL permit '+str(net4[0])+' '+str(net4.prefixlen)+'\n'
                    rollback = rollback + 'undo ip ip-prefix FTTH-V4-POOL permit '+str(net4[0])+' '+str(net4.prefixlen)+'\n'
                    vuelta += 1
                vuelta = 1
                aclSus = ""
                for pool in arraypoolSus:
                    postChecks = postChecks + "display bgp routing-table peer <DN1_LOOPBACK> advertised-routes " + pool.split("/")[0] + " " + pool.split("/")[1] + " \n"
                    net4 = ipaddress.IPv4Network(pool.strip())
                    if vuelta >= 10:
                        configuration = configuration + 'ip pool ftth_'+ agg.lower() + '_sus_o4' + str(vuelta) + ' bas remote\n'
                        resultPoolSus = resultPoolSus + 'ip-pool ftth_'+ agg.lower() + '_sus_o4' + str(vuelta) + '\n' 
                        rollbackPool = rollbackPool + 'ip-pool ftth_'+ agg.lower() + '_sus_o4' + str(vuelta) + '\n' 
                    else:
                        configuration = configuration + 'ip pool ftth_'+ agg.lower() + '_sus_o40' + str(vuelta) + ' bas remote\n'
                        resultPoolSus = resultPoolSus + 'ip-pool ftth_'+ agg.lower() + '_sus_o40' + str(vuelta) + '\n' 
                        rollbackPool = rollbackPool + 'ip-pool ftth_'+ agg.lower() + '_sus_o40' + str(vuelta) + '\n' 
                    configuration = configuration + ' gateway '+str(net4[-2])+' '+str(net4.prefixlen)+'\n'
                    configuration = configuration + ' dhcp-server group '+ agg.lower() + '_os4\n'
                    configuration = configuration + ' remote-ip lease manage\n'
                    configuration = configuration + '\n'
                    aclSus = aclSus  + '###\n'
                    aclSus = aclSus + 'acl number 3151\n'
                    aclSus = aclSus + ' rule ' + idLinea + ' deny ip source '+str(net4[0])+' '+str(net4.hostmask)+'\n'
                    ipprefix = ipprefix + 'ip ip-prefix FTTH-V4-POOL permit '+str(net4[0])+' '+str(net4.prefixlen)+'\n'
                    rollback = rollback + 'undo ip ip-prefix FTTH-V4-POOL permit '+str(net4[0])+' '+str(net4.prefixlen)+'\n'
                    vuelta += 1
                rollback = rollback + "aaa\n"
                rollback = rollback + '  domain ftth_'+ agg.lower() + '_od\n'
                rollback = rollback + '    undo ip-pool-group ftth_'+ agg.lower() + '_group_o\n'
                rollback = rollback + '    undo ipv6-pool ftth_'+ agg.lower() + '_o61\n'
                rollback = rollback + '  q\n'
                rollback = rollback + '  undo domain ftth_'+ agg.lower() + '_od\n'
                rollback = rollback + 'q\n'
                rollback = rollback + 'undo ip pool-group ftth_'+ agg.lower() + '_group_o\n\n'
                rollback = rollback + rollbackPool
                rollback = rollback + 'undo dhcp-server group '+ agg.lower() + '_os4\n'
                rollback = rollback + rollbackPrefix
                configuration = configuration + '  \n'
                configuration = configuration + 'ip pool-group ftth_'+ agg.lower() + '_group_o bas\n'
                configuration = configuration + ' warning-threshold 99\n'
                configuration = configuration + ' warning-exhaust\n'
                configuration = configuration + resultPoolFix
                configuration = configuration + resultPoolDyn
                configuration = configuration + resultPoolSus
                configuration = configuration + '#\n'
                configuration = configuration + '\n'
                configuration = configuration + aclSus
                configuration = configuration + 'q\n'
                configuration = configuration + '#\n'
                configuration = configuration + ipprefix 
                configuration = configuration + '\n'
                configuration = configuration + 'ip ip-prefix LOOPBACK2100_V4 index 5 permit ' +  str(giaddr)  + ' 32\n'
                configuration = configuration + '\n'
                configuration = configuration + 'route-policy DIRECT_V4_TO_BGP permit node ' + str(idRouteInt) + '\n'
                configuration = configuration + ' if-match ip-prefix LOOPBACK2100_V4\n'
                configuration = configuration + ' apply community ' + comm + '\n'
                configuration = configuration + ' \n'
                configuration = configuration + '#####AVISO - El equipo debe ya importar rutas directas. Solo para su revisión\n'
                configuration = configuration + '#bgp 12479\n'
                configuration = configuration + '#  ipv4-family unicast\n'
                configuration = configuration + '#    import-route direct route-policy DIRECT_V4_TO_BGP\n'
                configuration = configuration + '\n'
                configuration = configuration + '\n'
                configuration = configuration + '###Configuración IPv6. Ya debería estar el equipo configurado###\n'
                configuration = configuration + '#\n'
                configuration = configuration + '#	ipv6\n'
                configuration = configuration + '#	dhcp check-server-pkt loose\n'
                configuration = configuration + '#	undo dhcp through-packet\n'
                configuration = configuration + '#	undo dhcpv6 through-packet\n'
                configuration = configuration + '#	dhcpv6 duid llt\n'
                configuration = configuration + '#	Y\n'
                configuration = configuration + '#	dhcpv6-server keep-option 17\n'
                configuration = configuration + '#	dhcp conflict-ip-address offline user \n'
                configuration = configuration + '#	dhcpv6 conflict-ip-address offline user \n'
                configuration = configuration + '#	access-user dhcp auto-save max-user-number 16000\n'
                configuration = configuration + '#	access-user dhcp auto-recover enable\n'
                configuration = configuration + '#\n'
                configuration = configuration + '#	interface loopback 0\n'
                configuration = configuration + '#	  ipv6 enable\n'
                configuration = configuration + '#	  ipv6 add <GI-ADDRESS-O6> 128 \n'
                configuration = configuration + '#\n'
                configuration = configuration + '#	dhcpv6-server group '+ agg.lower() + '_os6\n'
                configuration = configuration + '#	 dhcpv6-server destination 2A01:C000:14::1 \n'
                configuration = configuration + '#	 dhcpv6-server destination 2A01:C000:14:1::1 \n'
                configuration = configuration + '#	 dhcpv6-server algorithm polling\n'
                configuration = configuration + '#	 dhcpv6-server source interface LoopBack0\n'
                configuration = configuration + '#	 dhcpv6 retransmit-solicit forward-mode through-packet\n'
                configuration = configuration + '#	 dhcpv6 retransmit-request forward-mode through-packet\n'
                configuration = configuration + '#	#\n'
                configuration = configuration + '#\n'
                configuration = configuration + '#	ipv6 prefix '+ agg.lower() + '_o61 remote\n'
                configuration = configuration + '#	 link-address <POOL_DINAMYC_OSP6>/44\n'
                configuration = configuration + '#	 remote-ip lease manage\n'
                configuration = configuration + '#	 dhcpv6-only\n'
                configuration = configuration + '#\n'
                configuration = configuration + '#	ipv6 pool '+ agg.lower() + '_o61 bas remote\n'
                configuration = configuration + '#	 prefix '+ agg.lower() + '_o61\n'
                configuration = configuration + '#	 dhcpv6-server group '+ agg.lower() + '_os6\n'
                configuration = configuration + '#	 \n'
                configuration = configuration + '#	ip ipv6-prefix LOOPBACK0_V6 index 5 permit <GI-ADDRESS-O6> 128 \n'
                configuration = configuration + '#	ip ipv6-prefix FTTH-V6-POOL index 5 permit <POOL_DINAMYC_OSP6> 44\n'
                configuration = configuration + '#	ipv6 route-static <POOL_DINAMYC_OSP6> 44 NULL0\n'
                configuration = configuration + '#\n'
                configuration = configuration + '#	route-policy DIRECT_V6_TO_BGP permit node 10\n'
                configuration = configuration + '#	 if-match ipv6 address prefix-list LOOPBACK0_V6\n'
                configuration = configuration + '#	 apply community ' + comm + '\n'
                configuration = configuration + '#	#\n'
                configuration = configuration + '#	ipv6 route-static vpn-instance net <POOL_DINAMYC_J6> 44 NULL0\n'
                configuration = configuration + '#	#\n'
                configuration = configuration + '#	route-policy STATIC_V6_TO_BGP permit node 10\n'
                configuration = configuration + '#	 if-match ipv6 address prefix-list FTTH-V6-POOL\n'
                configuration = configuration + '#	 apply community ' + comm + '\n'
                configuration = configuration + '#	#\n'
                configuration = configuration + '#	bgp 12479\n'
                configuration = configuration + '#	  ipv6-family unicast\n'
                configuration = configuration + '#	  import-route static route-policy STATIC_V6_TO_BGP\n'
                configuration = configuration + '#	  import-route direct route-policy DIRECT_V6_TO_BGP\n'
                configuration = configuration + '#\n'
                configuration = configuration + '###fin ipv6###\n'
                configuration = configuration + '\n'
                configuration = configuration + '\n'
                configuration = configuration + 'aaa\n'
                configuration = configuration + 'domain ftth_'+ agg.lower() + '_od  \n'
                configuration = configuration + ' authentication-scheme default0  / authentication-scheme non-authentication  (en x2)\n'
                configuration = configuration + ' #En X2 authentication-scheme non-authentication\n'
                configuration = configuration + ' accounting-scheme default0  \n'
                configuration = configuration + ' ip-pool-group ftth_'+ agg.lower() + '_group_o\n'
                configuration = configuration + ' ipv6-pool ftth_'+ agg.lower() + '_o61\n'
                configuration = configuration + ' trust upstream FTTH-WS\n'
                configuration = configuration + ' trust 8021p\n'
                configuration = configuration + ' user-priority upstream trust-8021p-outer \n'
                configuration = configuration + ' user-priority downstream trust-exp-outer  \n'
                configuration = configuration + ' user-group dhcpusergroup \n'
                configuration = configuration + ' access-user dhcp auto-save enable\n'
                configuration = configuration + ' \n'
                configuration = configuration + ' \n'
                for y in datos:
                    configuration = configuration + '#######configuración '+ str(y["OLT"]) +'##########\n\n'
                    configuration = configuration + 'interface Eth-Trunk'+ str(y["lag"]) +'.550\n'
                    configuration = configuration + ' description ACCESO | FTTH-WS | OSP | '+ str(y["OLT"]) +'\n'
                    configuration = configuration + ' 8021p 5\n'
                    configuration = configuration + ' user-vlan 550\n'
                    configuration = configuration + ' trust upstream OSP-WS\n'
                    configuration = configuration + ' dhcp dscp-outbound 46\n'
                    configuration = configuration + ' trust 8021p\n'
                    configuration = configuration + ' ipv6 enable\n'
                    configuration = configuration + ' ipv6 address auto link-local\n'
                    configuration = configuration + ' ipv6 nd autoconfig managed-address-flag\n'
                    configuration = configuration + ' ipv6 nd autoconfig other-flag\n'
                    configuration = configuration + ' bas\n'
                    configuration = configuration + ' #\n'
                    configuration = configuration + '  access-type layer2-subscriber default-domain authentication force ftth_'+ agg.lower() + '_od\n'
                    configuration = configuration + '  client-option82\n'
                    configuration = configuration + '  authentication-method bind\n'
                    configuration = configuration + '  arp-proxy\n'
                    configuration = configuration + '  user detect retransmit 5 interval 0\n'
                    configuration = configuration + ' #\n'
                    configuration = configuration + ' dhcpv6 relay option-insert interface-id mode tr-101\n'
                    configuration = configuration + '#\n'
                configuration = borradoDSG + configuration
                configuration = borradoPooles + configuration
                configuration = borradoDom + configuration
                configuration = borradoInt + configuration
                configuration = borradoCli + configuration
                configuration = borradoACL + configuration
                configuration = borradoPrefix + configuration
                configuration = configuration + postChecks
                configuration = configuration +  rollback
                configuration = checks +  configuration
                configuration = leaseTime + configuration
                files.write(configuration)
                aggAUX = str(ws1.cell(row = row,column = 3).value).strip().upper()
                TPAux = ws1.cell(row = row,column = 1).value